<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GREAT LOOKING AWESOMELY DESIGNED</title>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
</style>
<link rel="stylesheet" type = "text/css" href="css/modal.css">
<link rel="stylesheet" type = "text/css" href="css/post.css">
</head>
<body>

<h2>image file uploading app</h2>
<i class="fa fa-cloud"></i>
<!-- Trigger/Open The Modal -->
<button id="createBtn" class="button">Create New Post</button>

<table id="posts">
  <tr>
    <th>Title</th>
    <th>Thumbnail</th>
    <th>Filename</th>
    <th>Date Added</th>
    <th></th>
  </tr>
</table>
<!-- The Modal -->
<div id="createModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h2 id="headerTitle">Create Post</h2>
    </div>
    <div class="modal-body">
    <form enctype="multipart/form-data">  
    <label for="title"><b>Title</b></label><br/>
    <input type="text" placeholder="Enter Title" name="title" id="title"><br/>
    <label for="fileToUpload"><b>Filename</b></label><br/>
    <input name="fileToUpload" id="fileToUpload" type="file" /><br/>
    <input type="hidden" name="id" id="post_id">
    <input type="hidden" name="thumbnail" id="thumbnail">
    <input type="hidden" name="filename" id="filename">
    <img src="" id="thumb_img" style="display: none;"/>
    </form>
    </div>
    <div class="modal-footer">
      <button type="submit" class="submitbtn" id="submitbtn">Submit</button>
    </div>
  </div>

</div>

<script type="text/javascript" src="js/modal.js"></script>
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<script>
$( document ).ready(function() {

    GetAllPost();
    

    $('#createBtn').on('click', function () {
      $("#headerTitle").html("Add Post");
      $("#post_id").val("");
      $("#title").val("");
      $("#fileToUpload").val("");
      $("#thumb_img").hide();
      $("#thumb_img").attr("src","");
      $("#submitbtn").html("Submit");
    });

    $('.submitbtn').on('click', function () {

      if($('#title').val().length == 0){
        alert("Please Enter Title");
        return;
      }
      
      if($("#post_id").val().length == 0){
        if($('input:file').val().length == 0){
          alert("Please choose an image file to upload!");
          return;
        }
      }

      if($("#post_id").val() && $('input:file').val().length == 0){
        var id = $('#post_id').val();
        var title = $('#title').val();
        var filename = $('#filename').val();
        var thumbnail = $('#thumbnail').val();
        
        UpdatePost(id,title,filename,thumbnail);
        return;
      }

      $.ajax({
        // Your server script to process the upload
        url: 'upload.php',
        type: 'POST',

        // Form data
        data: new FormData($('form')[0]),

        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
          var id = $('#post_id').val();
          var title = $('#title').val();
          var filename = JSON.parse(result).filename;
          var thumbnail = JSON.parse(result).thumbnail;

          if($("#post_id").val()){
            UpdatePost(id,title,filename,thumbnail);
          }else{
            AddPost(title,filename,thumbnail);
          }
        }
      });
    });


 
});
function UpdatePost(id,title,filename,thumbnail){
        $.ajax({
        url: "api/update.php",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(
          {
            id : id,
            title : title,
            filename : filename,
            thumbnail : thumbnail
          }
        ),
        error: function (xhr, status) {
            alert(status);
        },
        success: function (result) {
          $("#createModal").hide();
          alert(result.message);           

          $(".post-data").empty();
          GetAllPost();
        }
    });
}

function EditPost(id){
  $("#createModal").show();

      $.ajax({
        url: "api/single_read.php/?id="+id,
        type: 'POST',
        dataType: 'json',
        error: function (xhr, status) {
            alert(status);
        },
        success: function (result) {
          console.log(result);    
          var id = result.id;
          var title = result.title;
          var filename = result.filename;
          var thumbnail = result.thumbnail;
          var created_at = result.created_at;
          
          $("#headerTitle").html("Edit Post");
          $("#post_id").val(id);
          $("#title").val(title);
          $("#filename").val(filename);
          $("#thumbnail").val(thumbnail);
          $("#fileToUpload").val("");
          $("#thumb_img").show();
          $("#thumb_img").attr("src","thumbnails/"+thumbnail);
          $("#submitbtn").html("Update");
        }
    });

}

function DeletePost(id){

  var resp = confirm("Do you want to delete this post?");

  if(resp == false) return;

      $.ajax({
        url: "api/delete.php",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(
          {
            id : id
          }
        ),
        error: function (xhr, status) {
            alert(status);
        },
        success: function (result) {
          alert(result.message);    

          $(".post-data").empty();

          GetAllPost();           
        }
    });
}

function AddPost(title,filename,thumbnail){
      $.ajax({
        url: "api/create.php",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(
          {
            title : title,
            filename : filename,
            thumbnail : thumbnail
          }
        ),
        error: function (xhr, status) {
            alert(status);
        },
        success: function (result) {
          $("#createModal").hide();
          alert(result.message);
           
          $(".post-data").empty();
          GetAllPost();          
        }
    });
}

function GetAllPost(){
        $.ajax({
        url: "api/read.php",
        type: 'POST',
        dataType: 'json',
        error: function (xhr, status) {
            alert(status);
        },
        success: function (result) {
            if(result.itemCount){               
                $.each( result.body, function( i, val ) {
                    var id = val.id;
                    var title = val.title;
                    var thumbnail = val.thumbnail;
                    var filename = val.filename;
                    var created_at = val.created_at;

                    var markup = "<tr class='post-data' id=post-" + id + "><td>" + title + "</td><td><img src=thumbnails/"+ thumbnail +" alt="+ title +"/></td><td><a href=uploads/" + filename +" target='_blank'>" + filename +"</a></td><td>" + created_at + "</td><td><button class='edit-post' id='edit-"+id+"' onclick='EditPost("+id+");return false;'>Edit</button> <button class='delete-post' id='delete-"+id+"' onclick='DeletePost("+id+");return false;'>Delete</button></td></tr>";
                    $("table tbody").append(markup);
                });

            }else{
                    var markup = "<tr class='post-data' style='text-align:center'><td colspan=5>No Post Found</td></tr>";
                    $("table tbody").append(markup);                
            }
        }
    });
}
</script>
</body>
</html>