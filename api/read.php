<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
    include_once '../config/database.php';
    include_once '../class/posts.php';

    $database = new Database();
    $db = $database->getConnection();

    $post = new Post($db);

    $stmt = $post->getAllPosts();
    $itemCount = $stmt->rowCount();


    //echo json_encode($itemCount);

    if($itemCount > 0){
        
        $postArr = array();
        $postArr["body"] = array();
        $postArr["itemCount"] = $itemCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "title" => $title,
                "thumbnail" => $thumbnail,
                "filename" => $filename,
                "created_at" => $created_at
            );

            array_push($postArr["body"], $e);
        }
        echo json_encode($postArr);
    }

    else{
        http_response_code(200);
        echo json_encode(
            array("message" => "No record found.","itemCount" => 0)
        );
    }
?>