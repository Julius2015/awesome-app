<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/database.php';
    include_once '../class/posts.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $post = new Post($db);
    
    $data = json_decode(file_get_contents("php://input"));
    
    $post->id = $data->id;
    
    // employee values
    $post->title = $data->title;
    $post->thumbnail = $data->thumbnail;
    $post->filename = $data->filename;
    $post->created_at = date('Y-m-d H:i:s');
    
    if($post->updatePostById()){
        http_response_code(201);
        echo json_encode(
            array("message" => "Post data updated.")
        );
    } else{
        http_response_code(409);
        echo json_encode(
            array("message" => "Data could not be updated.")
        );
    }
?>