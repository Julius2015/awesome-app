<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/database.php';
    include_once '../class/posts.php';

    $database = new Database();
    $db = $database->getConnection();

    $post = new Post($db);

    $post->id = isset($_GET['id']) ? $_GET['id'] : die();
  
    $post->getPostById();

    if($post->title != null){
        // create array
        $postArr = array(
            "id" =>  $post->id,
            "title" => $post->title,
            "thumbnail" => $post->thumbnail,
            "filename" => $post->filename,
            "created_at" => $post->created_at
        );
      
        http_response_code(200);
        echo json_encode($postArr);
    }
      
    else{
        http_response_code(404);
        echo json_encode("Post not found.");
    }
?>