<?php
$target_dir = "uploads/";
$target_thumbnail_dir = "thumbnails/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
  if($check !== false) {
    //echo "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    //echo "File is not an image.";
    $uploadOk = 0;
  }
}

// Check if file already exists
if (file_exists($target_file)) {
  //echo "Sorry, file already exists.";
  $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
  //echo "Sorry, your file is too large.";
  $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
  //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  http_response_code(409);  
  echo json_encode(
    array("message" => "Sorry, your file was not uploaded.")
  );
// if everything is ok, try to upload file
} else {
    $basename = md5(uniqid(rand(), true));
    $newFileName = $basename . '.' . strtolower(pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION));
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir . $newFileName)) {
    $thumb_image = createThumbnail($target_dir . $newFileName,$basename,$target_thumbnail_dir);

    http_response_code(201);  
    echo json_encode(
        array("filename" => $newFileName,
              "thumbnail" => $thumb_image)
    );       
} else {
    http_response_code(409);  
    echo json_encode(
        array("message" => "Sorry, there was an error uploading your file.")
    );      
  }
}


function createThumbnail($file,$basename,$folderPath){
        $sourceProperties = getimagesize($file);
        $fileNewName = $basename;
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];

        switch ($imageType) {

            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagepng($targetLayer,$folderPath. $fileNewName. "_thumbnail.". $ext);
                break;

            case IMAGETYPE_GIF:
                $imageResourceId = imagecreatefromgif($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagegif($targetLayer,$folderPath. $fileNewName. "_thumbnail.". $ext);
                break;

            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagejpeg($targetLayer,$folderPath. $fileNewName. "_thumbnail.". $ext);
                break;

            default:
                return "error.png";
                exit;
                break;

        }

        move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
        return $fileNewName. "_thumbnail.". $ext;
        //echo "Image Uploaded with created thumbnail Successfully.";
}

function imageResize($imageResourceId,$width,$height) {
    $targetWidth =60;
    $targetHeight =60;

    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
    return $targetLayer;
}
?>