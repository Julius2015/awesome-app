**A simple GREAT LOOKING AWESOMELY DESIGNED image file uploading app.**

A page that have a table with the following columns:

* Title
* Thumbnail
* Filename
* Date added

The page could:

* Add, edit, and delete records
* Adding record via popup modal form
* Record add, update, and delete should show without refreshing the page

---

## Create A Table

```
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `thumbnail` varchar(100) DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
```
---

## Clone the repository in in working environment

```
git clone https://juliusestebar@bitbucket.org/Julius2015/awesome-app.git
```

## Clone the repository to specific folder name in your working environment 

```
git clone https://juliusestebar@bitbucket.org/Julius2015/awesome-app.git awesome
```

---

## Configuring your Database 
( config/database.php)

```
private $host = "localhost";
private $database_name = "awesome";
private $username = "root";
private $password = "";
```
## Configuring your Tables 
( class/posts.php)
```
private $db_table = "posts";
```
---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

