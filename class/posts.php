<?php
    class Post{

        /**
         * Connection
         *
         * @var [type]
         */
        private $conn;

        /**
         * Table
         *
         * @var string
         */
        private $db_table = "posts";

        /**
         * Columns
         *
         * @var [type]
         */
        public $id;
        public $title;
        public $thumbnail;
        public $filename;
        public $created_at;
        
        /**
         * DB Connection
         *
         * @param [type] $db
         */
        public function __construct($db){
            $this->conn = $db;
        }

        /**
         * Get all Post
         *
         * @return void
         */
        public function getAllPosts(){
            $sqlQuery = "SELECT id, title, thumbnail, filename, created_at FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        /**
         * Create a Post
         *
         * @return void
         */
        public function createPost(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        title = :title, 
                        thumbnail = :thumbnail, 
                        filename = :filename, 
                        created_at = :created_at";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->title=htmlspecialchars(strip_tags($this->title));
            $this->thumbnail=htmlspecialchars(strip_tags($this->thumbnail));
            $this->filename=htmlspecialchars(strip_tags($this->filename));
            $this->created_at=htmlspecialchars(strip_tags($this->created_at));
        
            // bind data
            $stmt->bindParam(":title", $this->title);
            $stmt->bindParam(":thumbnail", $this->thumbnail);
            $stmt->bindParam(":filename", $this->filename);
            $stmt->bindParam(":created_at", $this->created_at);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        /**
         * Update Post By ID
         *
         * @return void
         */
        public function updatePostById(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        title = :title, 
                        thumbnail = :thumbnail, 
                        filename = :filename, 
                        created_at = :created_at
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->title=htmlspecialchars(strip_tags($this->title));
            $this->thumbnail=htmlspecialchars(strip_tags($this->thumbnail));
            $this->filename=htmlspecialchars(strip_tags($this->filename));
            $this->created_at=htmlspecialchars(strip_tags($this->created_at));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":title", $this->title);
            $stmt->bindParam(":thumbnail", $this->thumbnail);
            $stmt->bindParam(":filename", $this->filename);
            $stmt->bindParam(":created_at", $this->created_at);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        /**
         * Get Post By ID
         *
         * @return void
         */
        public function getPostById(){
            $sqlQuery = "SELECT
                        id, 
                        title, 
                        thumbnail, 
                        filename, 
                        created_at
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(1, $this->id);

            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->title = $dataRow['title'];
            $this->thumbnail = $dataRow['thumbnail'];
            $this->filename = $dataRow['filename'];
            $this->created_at = $dataRow['created_at'];
        }

        /**
         * Delete Post By ID
         *
         * @return void
         */
        public function deletePostById(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>